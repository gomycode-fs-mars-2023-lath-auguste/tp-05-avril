/**********************Exo 1************************ */
let result = 0;
function sum(a, b){
    return a + b;
}

result = sum(2, 3);

console.log(`La somme est ${result}`);

/**********************Exo 2************************ */
let result2 = 0;
function sumTab(tableau){
    let somme = 0;
    for(let i = 0; i < tableau.length; ++i){
        somme += tableau[i];
    }
    return somme;
}

result2 = sumTab([2,5,6]);
console.log(`La somme est ${result2}`);

/**********************Exo 2 bis************************ */
let result3 = 0;
function sumTab2(tableau){
    let somme = 0;
    for(let value of tableau){
        somme += value;
    }
    return somme;
}

result3 = sumTab2([2,5,6]);
console.log(`La somme avec la boucle for ... of est ${result3}`);

/**********************Exo 4************************ */
let result4 = 0;
function sumTabPos(tableau){
    let somme = 0;
    for(let value of tableau){
        if(value > 10){
            somme += value;
        }
    }
    return somme;
}

result4 = sumTabPos([2,5,6,11,14]);
console.log(`La somme avec la boucle for ... of est ${result4}`);

/**********************Exo 5************************ */


function tabRange(tab){
    let rangement = [];
    for(let value of tab){
        if(value > 0){
            rangement.push(++value);
        }
        else{
            rangement.push(--value);
        }
    }
    return rangement;
}

let result5 = tabRange([2,5,6,-6,-9])
console.log(result5)

/**********************Exo 6************************ */
function testChaine(tabcar){
    newChaine = [];
    for(value of tabcar){
        if(value == value.toLowerCase()){
            value = value.charAt(0).toUpperCase() + value.slice(1)
            newChaine.push(value)
        }
        else if(value == value.toUpperCase()){
            newChaine.push(value.toLowerCase())
        }
        else{
            value = value.charAt(0).toLowerCase() + value.slice(1).toUpperCase()
            newChaine.push(value)
        }
    }
    return newChaine;
}

// let liste = ['bonjour', 'salut', 'HELLO', 'Hi']
let result6 = testChaine(['bonjour', 'salut', 'HELLO', 'Hi']);
console.log(result6);

// console.log(liste[0].toUpperCase())