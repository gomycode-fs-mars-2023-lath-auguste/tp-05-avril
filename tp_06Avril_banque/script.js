/* Notre script gere les operations basiques d'une banque
    Nous avons comme entités :
        Clients
        Compte
        Transaction
*/


// Declaration des variables

let clients = [];
let comptes = [];
let transactions = [];


// Declaration de fonction

function addClient(){
    // recup des info clients puis ajout
    let nom = prompt("Nom : ");
    let email = prompt("email : ");
    let client = {nom, email};
    clients.push(client);

    return client;
}



function createCompte(email){
    // creation de compte lié au client par email
    let solde = parseInt(prompt("Entrez le montant initial : "));
    let compte = {email, solde}
    comptes.push(compte);
    return compte;
}


function transaction(email){
    // gestion des diff operations de retrait ou depot
    let montant = 0;
    let user = comptes.find(compte => compte.email === email);
    let typeOperation = prompt("Entrez 1 pour un depot ou 2 pour un retrait");
    switch(typeOperation){
        case "1":
            montant = prompt("Entrez le montant du depot");
            user.solde += parseInt(montant);
            break;
        case "2":
            montant = prompt("Entrez le montant du retrait");
            if(user.solde < parseInt(montant)){
                alert('montant dispo insuffisant');
            }
            else {
                user.solde -= parseInt(montant);
            }
            break;
        default:
            alert("Mauvais numero");
            break;
    };
    return 'Votre solde : ' + user.solde;
}

console.log(addClient());
console.log(createCompte("test@gmail.com"));
console.log(transaction("test@gmail.com"));

console.log(clients);
console.log(comptes);
